students = ["Ai", "Bi", "Ci", "Di", "Ei"]

grades = [95,98,87,94,92]

#key=0
#while (key<5):
    #print(f"The grade of {students[key]} is {grades[key]}")
    #key+1


car = {
  "brand": "Ford",
  "model": "Mustang",
  "color": "blue",
  "year": 2020
}

print(f"I own a {car['brand']} {car['model']} and it was made in {car['year']}")
